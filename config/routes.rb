Rails.application.routes.draw do

  root 'welcome#home'
  get 'about'   => 'welcome#about'
  get 'signup' => 'users#new'
  get 'todo' => 'lists#index'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  get 'users/:id/lists' => 'users#lists', :as => :user_lists
  get '/lists' => 'lists#index', :as => :lists
  post '/lists/new' => 'lists#create', :as => :new_list
  post '/lists/:id/todos/:id' => 'todo#changeDone', :as => :changeDone

  resources :users
  resources :lists do
    resources :todos
  end


end
