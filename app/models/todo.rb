class Todo < ActiveRecord::Base
  belongs_to :list
  validates :body, presence: true, length: {minimum:2}
end
