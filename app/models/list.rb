class List < ActiveRecord::Base
  belongs_to :user
  has_many :todos, dependent: :destroy

  validates :user_id, presence: true
  validates :name, presence: true,length: {minimum: 2, maximum:120}

end
