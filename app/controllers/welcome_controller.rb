class WelcomeController < ApplicationController
  def home
    if logged_in?
      redirect_to :controller => 'lists', :action => 'index'
    end
  end

  def about
  end
end
