class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user,   only: [:edit, :update]

  def new
  	@user = User.new
  end

  def show
  	@user = User.find(params[:id])
  end

  def create
  	@user = User.new(user_params)
  	if @user.save
      log_in @user
      #notify user
  		redirect_to @user
  	else
  		render 'new'
  	end
  end

  def lists
    if (params[:id])
      @user = User.find(params[:id])
    else
      @user = current_user
    end

    @lists = List.where(:user_id => @user.id)

  end

  private
  def user_params
  	params.require(:user).permit(:name,:username,:email,:password)
  end

  def logged_in_user
    unless logged_in?
      store_location
      redirect_to login_url
    end
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless @user == current_user?(@user)
  end

end
