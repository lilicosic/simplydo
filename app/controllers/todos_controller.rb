class TodosController < ApplicationController
  def create
    @list = List.find(params[:list_id])
    @todo = @list.todos.create(todo_params)
    redirect_to list_path(@list)

  end
  def new

  end

  def update
    @list = List.find(params[:list_id])
    @todo = @list.todos.find(params[:id])
    if @todo.update_attribute(:done, true)
      redirect_to list_path(@list), :notice => "You did it!"
    else
      render 'new'
    end

  end

  def destroy
    @list = List.find(params[:list_id])
    @todo = @list.todos.find(params[:id])
    @todo.destroy
    redirect_to list_path(@list)

  end


  private
  def todo_params
    params.require(:todo).permit(:body, :done)
  end

end
