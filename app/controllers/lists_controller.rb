class ListsController < ApplicationController
  def index

  end

  def show
    @list = List.find(params[:id])
  end

  def new
    @list = List.new
  end

  def create
    @list = current_user.lists.build(list_params)
    if @list.save
      #success
      redirect_to @list
    else
      render 'new'
    end
  end

  def destroy

    @list = List.find(params[:id])
    @list.destroy
    redirect_to lists_path

  end

  private
  def list_params
    params.require(:list).permit(:name)
  end

end
