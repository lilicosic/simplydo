# Simply Do #

** A simple web based to-do project in Ruby on Rails.  **


Project Idea
=======
Create account, create categories of your tasks. Delete or complete your tasks. See your old completed tasks.

Technical Details
=======
Uses Rails 4.2.0 and ruby 2.1.4p265